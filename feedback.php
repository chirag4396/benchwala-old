<?php 
include 'header.php';
?>

<section id="contact" class="">
	<div class="section-content">
		<h1 class="section-header"> <span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> Feedback Form</span></h1>
	</div>
	<div class="contact-section feed">
		<div class="container">
			<form id="feedform" name="feedform">
				<div class="col-md-6 col-xs-12 form-line">
					<div class="form-group">
						<label for="exampleInputUsername">Your name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder=" Enter Name" required>
					</div>
					<div class="form-group">
						<label for="exampleInputEmail">Email Address</label>
						<input type="email" class="form-control" name="email" id="email" placeholder=" Enter Email id" required>
					</div>  
					<div class="form-group">
						<label for="telephone">Mobile No.</label>
						<input type="tel" class="form-control" pattern="^\d{10}$" name="mobile" id="mobile" placeholder=" Enter 10-digit mobile no." required>
					</div>
				</div>
				<div class="col-md-6  col-xs-12 ">
					<div class="form-group">
						<label for ="description"> Message</label>
						<textarea  class="form-control" name="message" id="message" placeholder="Enter Your Message" required></textarea>
					</div>
					<div>

						<button type="submit" class="btn btn-default submit" id="btnadd" name="btnadd"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Send Message</button>
					</div>
					<center><font color="black"><div id="success_message" style="display:none;"></div></font></center>
				</div>
			</form>
		</div>
	</section>

	<?php include 'footer.php'; ?>
	<script src="js/custom/feedback.js"></script>
<footer id="fh5co-footer" role="contentinfo">
	<div class="">


		<div class="row copyright">
			<div class="col-md-12 text-center">
				<ul class="fh5co-social-icons col-md-4">
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
           
                <p class="col-md-4"></p>
				<p class="col-md-4">
					<?php 
					$res = $conn->query('select * from address_details where ad_city="PUNE(HO)"');
					if($res->num_rows){
                                    // $k = 0;
						while ($row = $res->fetch_assoc()) 
						{
							echo '<small class="block" style="
							color: #d1c286;
							font-size: 16px;
							line-height: 16px;
							float: right;
							">'.$row['ad_address']
							.'</small>';
						}
					}
					?>
					
				</p>


			</div>
		</div>



	</div>
	<p class="col-md-12 foo">

		<small class="block">2018 All Rights Reserved. Designed by <a style="line-height: 12px" href="http://sungare.com/" target="_blank">Sungare Technologies</a></small>
	</p>
</footer>
</div>

<div class="gototop js-top">
	<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>


<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="js/jquery.countTo.js"></script>
<!-- Flexslider -->
<script src="js/jquery.flexslider-min.js"></script>
<!-- Main -->
<script src="js/main.js"></script>

<script type="text/javascript" src="js/scroll.js"></script>

<script type="text/javascript">
	$(function() {
		$('#example').vTicker();
	});
</script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script src="js/custom/header.js"></script>
</body>
</html>
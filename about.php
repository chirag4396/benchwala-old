<?php
include 'header.php';
?>
<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" >
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
						<h1>About Us</h1>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="fh5co-about">
	<div class="container pad-t">
		<div class="about-content">
			<div class="row animate-box">
				<div class="col-md-6">
					<div class="desc">
						<!-- 	<h3>Company History</h3> -->
						<!--p>The furniture you choose for a room can make or break it. The furniture designs you choose are what determine the personality of the room and say something about the inhabitants of the house. Modern furniture design is all about finding the right balance between design and functionality, while traditional designs have evolved over time, reflecting the fashion fads of each period. Whether you are on the lookout for modern designs or more traditional styles, there are some fever time, reflecting the fashion fads of each period. Whether you are on the lookout for modern designs ver time, reflecting the fashion fads of each period. Whether you are on the lookout for modern designs or more traditional styles, there are some fever time, reflecting the fashion fads of each period. Whether you are on the lookout for modern designs or more traditional styles, there are some feor more traditional styles, there are some features to keep an eye out for.</p-->
						<p> Thank You For The Opportunity And Privilege To Introduce Our Products And Services Offered By Benchwala.Com.
                            We Are A Dedicated Team Of 100+ Emoloyees. We Are Leading Manufacturer And Distributor Of School Furniture In Pune. Established In 2005, Benchwala Today Has A Wide Variety Of School Furniture Which Includes</p>
     <p><li>Classroom Furniture </li>
    <li>Kids School Furniture</li>
    <li>Library Furniture</li>
    <li>Laboratory Furniture</li>
    <li>Auditorium Chairs</li>
    <li>Writing Pad Chairs</li>
    <li>Office Tables And Chairs</li>
    <li>Storage Solutions</li>
    <li>Hostel Furniture</li>
    <li>Computer Lab Furniture</li></p>
    
    <p>Our Clients Who Are Seeking Total Solution For Their Office, School And College Development Projects Are Welcome To Get In Touch With Us. We Provide Flexible End-To-End Solutions That Assist Educational Institutions To Meet Their Objectives. We Welcome Small And Large Projects And Will Be Happy To Quote You Free Of Any Charges Or Commitment. 
     We Are Available To Service Your Projects In Pune, Mumbai, Bangalore, Hyderabad, Delhi And Goa.</p>
					</div>
					
				</div>
				<div class="col-md-6">
					<img class="img-responsive" src="images/about.jpg" alt="about">
				</div>
			</div>
		</div>
		<div class="row">
			

			<?php 
			$res = $conn->query('select * from about_gallery');
			if($res->num_rows){
                                    // $k = 0;
				while ($row = $res->fetch_assoc()) 
				{
					?>
					<div class="col-md-4 col-sm-4 animate-box fadeIn animated-fast" data-animate-effect="fadeIn">
						<div class="fh5co-staff">
							<img src="uploads/<?php echo $row['ab_image']; ?>" alt="">
						</div>
					</div>
					<?php
				}
			}
			?>
			
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>
<?php 
include 'header.php'; 
?>
<div class="col-lg-3  col-md-12 m-t menu-bg" style="padding: 0;"><div class="col-md-12">
	<div class="widget category-list padding" style="border-right: none;">
		<h4 class="widget-header">All Category</h4>

		<ul class="category-list">
			<?php 
			$res = $conn->query('select * from category');
			if($res->num_rows){
                                    // $k = 0;
				while ($row = $res->fetch_assoc()) 
				{
					echo '<li><a href="category.php?id='.$row['cat_id'].'">'.$row['cat_name'].
					'</a></li>';
				}
			}
			?>

		</ul>
	</div>
</div></div>
<aside style="padding:0" class="col-md-12 col-lg-9 " id="fh5co-hero" class="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">
			<!--li style="background-image: url('images/1.png'); width: 100%; display: block; z-index: 2; height: 500px;" class="flex-active-slide" data-thumb-alt="">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-6 col-md-offset-3 col-md-pull-3 slider-text animated fadeInUp">

					</div>
				</div>
			</li-->
			<li style="background-image: url('images/2.jpg'); width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
				<div class="container">
					<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

					</div>
				</div>
			</li>
			<li style="background-image: url('images/3.png');width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
				<div class="container">
					<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

					</div>
				</div>
			</li>
			<li style="background-image: url('images/4.jpg');width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
				<div class="container">
					<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

					</div>
				</div>
			</li>
		</ul>
		<ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>
	</aside>

	<div id="fh5co-services" class="fh5co-bg-section">

		<div class="container">
			<h2 class="head1">Explore Our Furniture </h2>

			<div class="border-line3"></div>

			<div class="row">
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/11.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/12.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/13.jpg"></a>




					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/14.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/15.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/16.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/17.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/18.jpg"></a>

					</div>
				</div>

			</div>

			<!-- 	<div class="col-md-12"><p><a href="#" class="btn btn-primary btn-outline">Learn More</a></p></div> -->
		</div>
	</div>
	<div id="fh5co-testimonial" class="fh5co-bg-section ">
		<div class="container">
			<div class="row animate-box fadeInUp animated-fast">
				<div class="col-md-8 col-md-offset-2 text-center">
					  
				</div> 
				</div>
			</div>

			<div class="carousel-reviews broun-block">
				<div class="container">
					<div class="row">
						<div id="carousel-reviews" class="carousel slide" data-ride="carousel">
							
							<div class="carousel-inner">
								<?php
								$qry = 'select * from testimonial';
								$res = $conn->query($qry);
								if($res->num_rows)
								{
									$k = 1;
									while ($row = $res->fetch_assoc()) {
										if($k == 1){
											echo '<div class="item active">';
										}
										?>

											<div class="col-md-4 col-sm-6">
												<div class="block-text rel zmin">

													<div class="agileits_w3layouts_testimonials_grid ">
									
									<?php										echo "<img src='uploads/".$row['test_image']."' class='img-responsive'  >";  ?>
													</div>
													<div class="clear"></div>
													<h3 style="text-align: center;" title="" href="#"><?php echo $row['test_name']; ?></h3>
													<h4><?php echo $row['test_designation']; ?></h4>
													<p><?php echo $row['test_description']; ?></p>
													<ins class="ab zmin sprite sprite-i-triangle block"></ins>
												</div>

											</div>
											<!-- <div class="col-md-4 col-sm-6">
												<div class="block-text rel zmin">

													<div class="agileits_w3layouts_testimonials_grid ">
														<img src="images/c1.png" alt=" " class="img-responsive">
													</div>
													<div class="clear"></div>
													<h3 style="text-align: center;" title="" href="#">Ponny Chacko</h3>
													<h4>(ui-designer)</h4>
													<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us by X-Men: The last Stand director Brett Ratner. If the name of the director wasn't enough to dissuade ...</p>
													<ins class="ab zmin sprite sprite-i-triangle block"></ins>
												</div>

											</div>
											<div class="col-md-4 col-sm-6">
												<div class="block-text rel zmin">

													<div class="agileits_w3layouts_testimonials_grid ">
														<img src="images/c1.png" alt=" " class="img-responsive">
													</div>
													<div class="clear"></div>
													<h3 style="text-align: center;" title="" href="#">Ponny Chacko</h3>
													<h4>(ui-designer)</h4>
													<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us by X-Men: The last Stand director Brett Ratner. If the name of the director wasn't enough to dissuade ...</p>
													<ins class="ab zmin sprite sprite-i-triangle block"></ins>
												</div>

											</div> -->
										
										<?php
										if($k % 3 == 0){
											
											echo '</div>'
											.'<div class="item">';
											
										}
										$k++;
									}
								}
								?>               
							</div>

						</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- </div> -->
</div>
</div>
<?php include 'footer.php'; ?>
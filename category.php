<?php 
include 'header.php';
$id = $_GET['id'];
?>
<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/q5.jpg);">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box" data-animate-effect="fadeIn">
						<h1>Products</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>



<div id="fh5co-product">
	<div class="row animate-box">
		<div class="col-md-12  text-center fh5co-heading">

			<p>Get a fresh, perfectly coordinated look for your Place with these gorgeous new designs.</p>

		</div>
	</div>

	<div class="container pro">

		<div class="row">
			<div class="col-md-3">
				<div class="widget category-list">
					<h4 class="widget-header">All Category</h4>
					<?php
					$sql = "SELECT * FROM category";
					$result = $conn->query($sql);

					while($row = $result->fetch_assoc()) {


						echo'<ul class="category-list">'
						.'<li><a href="category.php?id='.$row['cat_id'].'">'.$row['cat_name'].' </a></li>'
						.'</ul>';
					}

					?>
				</div>
			</div>
			<div class="col-md-9 ">
				<?php
				$sql = 'SELECT * FROM category where cat_id='.$id;
				$result = $conn->query($sql);
				$row = $result->fetch_assoc();

				?>
				<h4 class="widget-header col-md-8 col-xs-12"><?php echo $row['cat_name']; ?></h4>

				<div class="col-md-4 col-xs-12"><span style="color:red">Sort By</span>
					<select>

						<option value="1">Model No.</option>
						<option value="1">Price High to Low</option>
						<option value="1">Price Low to High</option>


					</select>
				</div>

				<div class="clear"></div>
				<h6><span style="color:red">Purchase Order:</span> We do accept PO from Office; Corporate; Institute & Hospitals.</h6>
				<?php
				$sql = 'SELECT * FROM category JOIN product_details ON cat_id = pro_cat_id where cat_id='.$id;
				$result = $conn->query($sql);
				if($res->num_rows){
					while($row = $result->fetch_assoc()) {

						?>
						<div class="col-md-4 text-center animate-box">
							<div class="product">
								<div class="product-grid" style="background-image:url(uploads/<?php echo $row['pro_image']; ?>);">
									<div class="inner">
										<p>
											<!-- <a href="single.php?id='.$row['pro_id'].'" class="icon"><i class="icon-shopping-cart"></i></a> -->
											<a href="single.php?id=<?php echo $row['pro_id']; ?>" class="icon"><i class="icon-eye"></i></a>
										</p>
									</div>
								</div>
								<div class="desc">
									<h3><a href="single.php?id=<?php echo $row['pro_id']; ?>"><?php echo $row['pro_model_no']; ?></a></h3>
									<button onclick="productEnquiry(<?php echo $row['pro_id']; ?>)" class="btn btn-primary btn-outline btn-lg">Enquiry</button>
								</div>
							</div>
						</div>
						<?php	
					}
				}
				?>
			</div>

		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog" >
	<div class="modal-dialog" ">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h2 class="head1">Enquiry</h2>
				<div class="border-line3"></div>
			</div>
			<div class="modal-body">
				<form name="myform" id="enquiryForm">
					<input type="hidden" name="id" id="id">
					<div class="row  form-group">	
						<div class="col-md-4">
							<label for="firstname"> Name <span>*</span></label>
						</div>
						<div class="col-md-8">
							<input type="text" name="firstname" placeholder="Name" class="form-control" required="">
									<!-- <input type="hidden" name="code" value="AU-002">
									<input type="hidden" name="cat_name" value="Auditorium Chair">
									<input type="hidden" name="pcat_pkey" value="318">
									<input type="hidden" name="list_price" value="11,000.00">
									<input type="hidden" name="your_price" value="6,875.00"> -->
								</div>
							</div>	

							<div class="row form-group">
								<div class="col-md-4">
									<label for="email"> Email<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="email" name="email" placeholder="Email" class="form-control" required="">
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Contact No<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="tel" name="mobile" pattern="^\d{10}$" placeholder="Contact No" class="form-control" required="">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Message<span>*</span></label>
								</div>
								<div class="col-md-8">
									<textarea type="text" name="message" placeholder="Enter Your message" class="form-control" required=""></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="form-group col-md-8 col-xs-12">
								<input type="submit" value="Send Message" class="btn btn-primary">
							</div>
						</div>
					</form>
					<div class="alert alert-warning text-md-center" id = "enquiryMessage" style="display: none;">				
					</div>
					<div class="clear"></div>
					<div class="top-header">
						<div class="">
							<div class="col-md-12" >
								<p>CALL:<?php $qry = 'select ad_city,ad_mobile from address_details';
									$res = $conn->query($qry);
									if($res->num_rows){
										while($row = $res->fetch_assoc())
										{
											echo $row['ad_city'].':'.$row['ad_mobile'].'|';
										}
									}  ?></p>
								</div>
								<div class="col-md-12">
									<p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - 
										<?php $qry = 'select ad_email from address_details where ad_city="PUNE(HO)"';
										$res = $conn->query($qry);
										if($res->num_rows){
											while($row = $res->fetch_assoc())
											{
												echo $row['ad_email'];
											}
										}  ?></p>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- jQuery -->
				<script src="js/custom/product.js"></script>
			</div>
			<?php
			include 'footer.php';
			?>
			
<?php
include 'header.php';
?>

<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/q5.jpg);">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box" data-animate-effect="fadeIn">
						<h1>Get in Touch</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>


<div id="fh5co-contact">
	<div class="container pad-t">
		<div class="row">
			<div class="col-md-5 col-md-push-1 animate-box fadeInUp animated-fast">

				<div class="fh5co-contact-info">
					<h3>Contact Information</h3>
					<ul>
						<?php 
						$res = $conn->query('select * from address_details where ad_city="PUNE(HO)"');
						if($res->num_rows){
                                    // $k = 0;
							while ($row = $res->fetch_assoc()) 
							{
								?>

								<li class="address" style="width: 80%;"><?php echo $row['ad_address']; ?></li>
								<li class="phone"><a href="tel:<?php echo $row['ad_mobile']; ?>"><?php echo $row['ad_mobile']; ?></a></li>
								<!-- <li class="phone"><a href="tel://1234567920">020-26932545 </br> 020-64001707 </br> <?php echo $row['ad_mobile']; ?></a></li> -->
								<li class="email"><a href="mailto:<?php echo $row['ad_email']; ?>"><?php echo $row['ad_email']; ?></a></li>
								<?php
							}
						}
						?>
					</ul>
				</div>

			</div>
			<div class="col-md-6 animate-box fadeInUp animated-fast">
				<h3>Get In Touch</h3>
				<form id="contactform">
					<div class="row form-group">
						<div class="col-md-6 mar-bb">
							<!-- <label for="fname">First Name</label> -->
							<input type="text" name="fname" id="fname" class="form-control" placeholder="Your firstname" required>
						</div>
						<div class="col-md-6">
							<!-- <label for="lname">Last Name</label> -->
							<input type="text" name="lname" id="lname" class="form-control" placeholder="Your lastname" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<!-- <label for="email">Email</label> -->
							<input type="email" name="email" id="email" class="form-control" placeholder="Your email address" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<!-- <label for="subject">Subject</label> -->
							<input type="text" name="subject" id="subject" class="form-control" placeholder="Your subject of this message" required>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-12">
							<!-- <label for="message">Message</label> -->
							<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something about us" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<input type="submit" name="btnadd" id="btnadd" value="Send Message" class="btn btn-primary">
					</div>
					<center><font color="black"><div id="success_message" style="display:none;">Data Submitted Sucessfully </div></font></center>
				</form>		
			</div>
		</div>

	</div>
</div>
<div class="col-md-12 text-center">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242117.68101783327!2d73.72287860996965!3d18.524890422025425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1520080104290" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php include 'footer.php'; ?>
<script src="js/custom/contact.js"></script>
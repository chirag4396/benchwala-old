<?php 
include 'header.php'; 
function dropdown_change(){
	$sort=array('Modal No'=>1,'Price High To Low'=>2,'Price Low To High'=>3);
	$options='';
	while(list($k,$v)=each($sort)){
		$options.='<option value="'.$v.'">'.$k.'</option>';
	}
	return $options;
}
function fetch_product($conn){
	$output='';
	$res = $conn->query('select * from product_details inner join category on category.cat_id = product_details.pro_cat_id');

	if($res->num_rows){

		while ($row = $res->fetch_assoc() ) {
			$Sql = 'select * from product_details where pro_id = '.$row['pro_id'].' limit 0,1';

			$res1 = $conn->query($Sql);
			$row1 = $res1->fetch_assoc();
			while($row1 = $res1->fetch_assoc()){
				$output .='<div class="product">'
				.'<div class="product-grid" style="background-image:url(uploads/'.$row1['pro_image'].'">'
				.'<div class="inner">'
				.'<p>'
				.'<a href="single.php?id='.$row1['pro_id'].'" class="icon"><i class="icon-eye"></i></a>'
				.'</p>'
				.'</div>'
				.'</div>'
				.'<div class="desc">'
				.'<h3><a href="single.php?id='.$row1['pro_id'].'">'.$row1['pro_model_no'].'</a></h3>
				<button onclick="productEnquiry('.$row1['pro_id'].')" class="btn btn-primary btn-outline btn-lg">Enquiry</button>'
				.'</div>'
				.'</div>';
			}
			return $output;
		}
	}
}
?>
<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/q5.jpg);">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box" data-animate-effect="fadeIn">
						<h1>Products</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div id="fh5co-product">
	<div class="row animate-box">
		<div class="col-md-12  text-center fh5co-heading">

			<p>Get a fresh, perfectly coordinated look for your Place with these gorgeous new designs.</p>

		</div>
	</div>

	<div class="container pro">

		<div class="row">
			<div class="col-md-3">
				<div class="widget category-list">
					<h4 class="widget-header">All Category</h4>
					<ul class="category-list">
						<?php 
						$res = $conn->query('select * from category');
						if($res->num_rows){
                                    // $k = 0;
							while ($row2 = $res->fetch_assoc()) 
							{
								echo '<li><a href="product.php?id='.$row2['cat_id'].'">'.$row2['cat_name'].
								'</a></li>';
							}
						}
						?>
					</ul>
				</div>
			</div>
			<div class="col-md-9 ">
				<h4 class="widget-header col-md-8 col-xs-12">KIDS SCHOOL
					FURNITURE</h4>

					<div class="col-md-4 col-xs-12"><span style="color:red">Sort By</span>
						<select>
							<option value="">Select Option</option>
							<?php echo dropdown_change(); ?>

						</select>
					</div>

					<div class="clear"></div>
					<h6><span style="color:red">Purchase Order:</span> We do accept PO from Office; Corporate; Institute & Hospitals.</h6>


					<div class="col-md-4 text-center animate-box">
						<?php echo fetch_product($conn); ?>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<footer id="fh5co-footer" role="contentinfo">
	<div class="">


		<div class="row copyright">
			<div class="col-md-12 text-center">
				<ul class="fh5co-social-icons col-md-3">
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
				</ul>


				<p class="col-md-9">

					<small class="block" style="
					color: #d1c286;
					font-size: 16px;
					line-height: 16px;
					">Sr. No. 33/1.Khadi machine chowk,
					near HP petrol pump,
					opp. Telephone  Exchange office,
					kondhwa BK, Pune 33. </small>
				</p>


			</div>
		</div>



	</div>
	<p class="col-md-12 foo">

		<small class="block">2018 All Rights Reserved. Designed by <a style="line-height: 12px" href="http://sungare.com/" target="_blank">Sungare Technologies</a></small>
	</p>
</footer>
</div>

<div class="gototop js-top">
	<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>


<div id="myModal" class="modal fade" role="dialog" >
	<div class="modal-dialog" ">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h2 class="head1">Enquiry</h2>
				<div class="border-line3"></div>
			</div>
			<div class="modal-body">
				<form name="myform" id="enquiryForm">
					<input type="hidden" name="id" id="id">
					<div class="row  form-group">	
						<div class="col-md-4">
							<label for="firstname"> Name <span>*</span></label>
						</div>
						<div class="col-md-8">
							<input type="text" name="firstname" placeholder="Name" class="form-control" required="">
									<!-- <input type="hidden" name="code" value="AU-002">
									<input type="hidden" name="cat_name" value="Auditorium Chair">
									<input type="hidden" name="pcat_pkey" value="318">
									<input type="hidden" name="list_price" value="11,000.00">
									<input type="hidden" name="your_price" value="6,875.00"> -->
								</div>
							</div>	

							<div class="row form-group">
								<div class="col-md-4">
									<label for="email"> Email<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="email" name="email" placeholder="Email" class="form-control" required="">
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Contact No<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="tel" name="mobile" placeholder="Contact No" class="form-control" required="">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Message<span>*</span></label>
								</div>
								<div class="col-md-8">
									<textarea type="text" name="message" placeholder="Enter Your message" class="form-control" required=""></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="form-group col-md-8 col-xs-12">
								<input type="submit" value="Send Message" class="btn btn-primary">
							</div>
						</div>
					</form>
					<div class="alert alert-warning text-md-center" id = "enquiryMessage" style="display: none;">				
					</div>
					<div class="clear"></div>
					<div class="top-header">
						<div class="">
							<div class="col-md-12" >
								<p>CALL:<?php $qry = 'select ad_city,ad_mobile from address_details';
									$res = $conn->query($qry);
									if($res->num_rows){
										while($row = $res->fetch_assoc())
										{
											echo $row['ad_city'].':'.$row['ad_mobile'].'|';
										}
									}  ?></p>
								</div>
								<div class="col-md-12">
									<p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - support@benchwala.com</p>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- jQuery -->
			<script src="js/jquery.min.js"></script>
			<!-- jQuery Easing -->
			<script src="js/jquery.easing.1.3.js"></script>
			<!-- Bootstrap -->
			<script src="js/bootstrap.min.js"></script>
			<!-- Waypoints -->
			<script src="js/jquery.waypoints.min.js"></script>
			<!-- Carousel -->
			<script src="js/owl.carousel.min.js"></script>
			<!-- countTo -->
			<script src="js/jquery.countTo.js"></script>
			<!-- Flexslider -->
			<script src="js/jquery.flexslider-min.js"></script>
			<!-- Main -->
			<script src="js/main.js"></script>
			<script src="js/custom/product.js"></script>
		</body>
		</html>
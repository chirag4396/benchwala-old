<nav class="main-menu">
	<ul>
		<li>
			<a href="dashborad.php">
				<i class="fa fa-home nav_icon"></i>
				<span class="nav-text">
					Dashboard
				</span>
			</a>
		</li>

		<li>
			<a href="testimonial.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Testimonial
				</span>
			</a>
		</li>
		<li>
			<a href="category.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Category
				</span>
			</a>
		</li>
		<li>
			<a href="aboutus_gallery.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					About Us Gallary
				</span>
			</a>
		</li>
		<li>
			<a href="gallery.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Gallary
				</span>
			</a>
		</li>
		<li>
			<a href="product.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					product
				</span>
			</a>
		</li>

		<li>
			<a href="contact.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Contact Enquiry
				</span>
			</a>
		</li>
		<li>
			<a href="feedback.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Customer Feedback
				</span>
			</a>
		</li>
		<li>
			<a href="address.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Address Details
				</span>
			</a>
		</li>
		<li>
		<a href="product_enquiry.php">
				<i class="icon-table nav-icon"></i>
				<span class="nav-text">
					Product Enquiry
				</span>
			</a>
		</li>

	</ul> 
	<ul class="logout">
		<li>
			<a href="logout.php">
				<i class="icon-off nav-icon"></i>
				<span class="nav-text">
					Logout
				</span>
			</a>
		</li>
	</ul>
</nav>
<?php
include("../config.php");
?>
<!DOCTYPE html>
<head>
	<title>Benchwala</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>
	<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>

	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	
	
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- tables -->
				
				<div class="table-heading">
					<h2></h2>
				</div>
				<div class="agile-tables">
					<div class="w3l-table-info">
						<h3>Address Details</h3>
						<div class="col-sm-12">
							<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" id = "addNew">Add New Record</button>
							<!-- <button class="btn btn-danger" id = "deleteAll">Delete All</button> -->
							<div class="clear"></div>
						</div>	
						<table id="addressTable">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>City</th>
									<th>Mobile No.</th>
									<th>Address</th>
									<th>Email Id</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>

						</table>

			 <!-- <code class="js">
					$('#table').basictable();
				  </code>
				-->

			</div>
			<!-- //tables -->
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form id = "addressForm">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Address Form</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="hidden" id = "id" name = "id">
						<div class="form-group">
							<label for="exampleInputEmail1">City</label>
							<input type="text" name = "city" id="city" placeholder="Enter City" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Contact No.</label>
							<input type="number" name="mobile" class="form-control" id="mobile" placeholder="Enter Mobile" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Address</label>
							<textarea  name="address" class="form-control" id="address" placeholder="Enter Address" required></textarea>
							<script>
								CKEDITOR.replace( 'address' );
							</script>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email Id.</label>
							<input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Id" required>
						</div>
					</div>
					<div class="modal-footer">						
						<button type="submit" class="btn btn-primary" id = "addBtn">Add</button>
						<button type="button" class="btn btn-primary" id = "upBtn">Update</button>
					</div>
				</form>
				
				<div class="alert alert-warning text-md-center" id = "addressMessage" style="display: none;">				
				</div>

			</div>
		</div>
	</div>

	<div id="dModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body text-center">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p>Are you sure ?</p>	        
					<button type="button" class="btn btn-danger" id = "yes">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
					<div id = "dAlert" class="alert text-center hidden"></div>
				</div>
			</div>

		</div>
	</div>
	<!-- footer -->
	<?php require("footer.php") ?>
	<!-- //footer -->
</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	

<script src="js/bootstrap.js"></script>

<script src="../admin/js/custom/address.js"></script>
</body>
</html>


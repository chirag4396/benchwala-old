fetchData();
function fetchData(){
    $.ajax({
        url : 'req/address/fetch_address.php',
        type : 'post',
        data : '',
        dataType : 'json',
        success : function(data){
            // console.log(data);
            trArr = [];

            $.each(data,function(key,val){
                tr = '<tr id = "tr-'+val.ad_id+'">'
                +'<td>'+val.ad_id+'</td>'
                +'<td>'+val.ad_city+'</td>'
                +'<td>'+val.ad_mobile+'</td>'
                +'<td>'+val.ad_address+'</td>'
                +'<td>'+val.ad_email+'</td>'
                +'<td>'
                +'<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getData('+val.ad_id+')">Edit</button>|'
                +'<button class="btn btn-danger" onclick="delete_address('+val.ad_id+')">DELETE</button>'
                +'</td>'
                +'</tr>';
                trArr.push(tr);
            });

            $('#addressTable tbody').html(trArr);
        }
    });

}
function response(data,msg){
    if(data.indexOf('OK') >= 0){
        $('#addressMessage').show().html(msg);          
        fetchData();
        $('#addressForm')[0].reset();

        window.setTimeout(function(){
            $('#myModal').modal('toggle');
            $('#addressMessage').hide().html('');
        },2000);
    }else{
        $('#addressMessage').show().html('Something Went Wrong');
        window.setTimeout(function(){
            $('#addressMessage').hide().html('');
        },3000);
    }
}

$('#addNew').on({
    'click' : function(){
        $('#addressForm')[0].reset();

        $('#addBtn').show();
        $('#upBtn').hide();
    }
});
$('#addressForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#addressMessage').fadeIn().html('Checking Data');
        var formdata= new FormData(this);
        formdata.append('address',CKEDITOR.instances['address'].getData());
        $.ajax({

            url : 'req/address/add_address.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
                // console.log(data);           
                response(data,'Successfully Inserted');
            fetchData();
        }   
    });
    }
});

function getData(id){
    $('#addBtn').hide();
    $('#upBtn').show();
    // $('#category').show();
    $.ajax({
        url : 'req/address/addressDetails.php',
        type : 'post',
        dataType : 'json',
        data : {'id':id},
        success : function(data){
            // console.log(data);
            $('#id').val(data.ad_id);
            $('#city').val(data.ad_city);
            $('#mobile').val(data.ad_mobile);
            $address = CKEDITOR.instances['address'].setData(data.ad_address);
            // $('#address').val(data.ad_address);
            $('#email').val(data.ad_email);
        }
    });
}

$('#upBtn').on({
    'click' : function(e){
        e.preventDefault();
        var formdata = new FormData($('#addressForm')[0]);
        formdata.append('address',CKEDITOR.instances['address'].getData());
        $.ajax({
            url : 'req/address/update_address.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
            // console.log(data);   
            response(data,'Successfully Updated');
            fetchData();
        }   
    });
    }
});

function delete_address(id) {
    $('#dModal').modal('toggle');
    $('#yes').attr('onclick','del('+id+')');

}

function del(id){
    $.post('req/address/delete_address.php', {ad_id : id}, function(data){
        //if(data == 'OK'){
            if(data.indexOf("1") >= 0 ){
                $('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
                window.setTimeout(function(){
                    $('#dModal').modal('toggle');
                    $('#tr-'+id).remove();
                },1000);
            }else{
                $('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');         
            }

            window.setTimeout(function(){
                $('#dAlert').addClass('hidden').html('');
            },1000);
        });
}
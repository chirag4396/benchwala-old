fetchData();
function fetchData(){
    $.ajax({
        url : 'req/category/fetch_category.php',
        type : 'post',
        data : '',
        dataType : 'json',
        success : function(data){
            // console.log(data);
            trArr = [];

            $.each(data,function(key,val){
                tr = '<tr id = "tr-'+val.cat_id+'">'
                +'<td>'+val.cat_id+'</td>'
                +'<td><img src = "../uploads/'+val.cat_img+'" width="100" class="img-thumbnail"></td>'
                +'<td>'+val.cat_name+'</td>'
                +'<td>'+val.cat_description+'</td>'
                +'<td>'
                +'<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getData('+val.cat_id+')">Edit</button>|'
                +'<button class="btn btn-danger" onclick="delete_category('+val.cat_id+')">DELETE</button>'
                +'</td>'
                +'</tr>';
                trArr.push(tr);
            });

            $('#categoryTable tbody').html(trArr);
        }
    });

}
function response(data,msg){
    if(data.indexOf('OK') >= 0){
        $('#categoryMessage').show().html(msg);          
        fetchData();
        $('#categoryForm')[0].reset();

        window.setTimeout(function(){
            $('#myModal').modal('toggle');
            $('#categoryMessage').hide().html('');
        },2000);
    }else{
        $('#categoryMessage').show().html('Something Went Wrong');
        window.setTimeout(function(){
            $('#categoryMessage').hide().html('');
        },3000);
    }
}

$('#addNew').on({
    'click' : function(){
        $('#categoryForm')[0].reset();

        $('#addBtn').show();
        $('#upBtn').hide();
    }
});
$('#categoryForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#categoryMessage').fadeIn().html('Checking Data');
        var formdata= new FormData(this);
         formdata.append('desc',CKEDITOR.instances['desc'].getData());
        $.ajax({

            url : 'req/category/add_category.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
                console.log(data);           
                response(data,'Successfully Inserted');
                fetchData();
            }   
        });
    }
});

function getData(id){
    $('#addBtn').hide();
    $('#upBtn').show();
    // $('#category').show();
    $.ajax({
        url : 'req/category/categoryDetails.php',
        type : 'post',
        dataType : 'json',
        data : {'id':id},
        success : function(data){
            // console.log(data);
            $('#id').val(data.cat_id);
            $('#category').val(data.cat_name);
            $desc = CKEDITOR.instances['desc'].setData(data.cat_description);
            $('#imagePreview').attr('src','../../uploads/'+data.cat_img);           
        }
    });
}

$('#upBtn').on({
    'click' : function(e){
        e.preventDefault();
        var formdata = new FormData($('#categoryForm')[0]);
        formdata.append('desc',CKEDITOR.instances['desc'].getData());
        $.ajax({
            url : 'req/category/update_category.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
              console.log(data);   
            response(data,'Successfully Updated');
            fetchData();
        }   
    });
    }
});
function delete_category(id) {
    console.log(id);
    $('#dModal').modal('toggle');
    $('#yes').attr('onclick','del('+id+')');

}

function del(id){
    console.log(id);
    $.post('req/category/delete_category.php', {cat_id : id}, function(data){
        /*if(data == 'OK'){*/
            if(data.indexOf("1") >= 0 ){
                $('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
                window.setTimeout(function(){
                    $('#dModal').modal('toggle');
                    $('#tr-'+id).remove();
                },1000);
            }else{
                $('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');         
            }

            window.setTimeout(function(){
                $('#dAlert').addClass('hidden').html('');
            },1000);
        });
}
var height,width;

function imageUpload(id) {

    $('#'+id).on("change", function(){
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) return;

        if (/^image/.test( files[0].type)){
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            var sizeMB = files[0].size / (1024 * 1024);
            if(sizeMB > 2){
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', true);
                $('#'+id).parent().append('<p style = "color: red;margin-top: 10px;">Please select lessthan 2 MB Image.</p>');
            }else{
                $('#'+id).parent().find('p').remove();
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', false);             
            }
            reader.onloadend = function(upImg){

                $("#"+id+"Preview").attr("src", this.result);
                urlValue = this.result;
                
                var image = new Image();
                image.src = upImg.target.result;

                image.onload = function() {
                    height = this.height;
                    width = this.width;                 
                };
            }                
        }
    });
}

imageUpload('image');
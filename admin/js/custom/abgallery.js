fetchData();
function fetchData(){
    $.ajax({
        url : 'req/fetch_abgallery.php',
        type : 'post',
        data : '',
        dataType : 'json',
        success : function(data){
            // console.log(data);
            trArr = [];

            $.each(data,function(key,val){
                tr = '<tr id = "tr-'+val.ab_id+'">'
                +'<td>'+val.ab_id+'</td>'
                +'<td>'+val.ab_tittle+'</td>'
                +'<td>'+'<img src="../uploads/'+val.ab_image+'" width="100" class="img-thumbnail"/>'+'</td>'
                +'<td>'
                +'<button class="btn btn-danger" onclick="delete_abgallery('+val.ab_id+')">DELETE</button>'
                +'</td>'
                +'</tr>';
                trArr.push(tr);
            });

            $('#countTable tbody').html(trArr);
        }
    });

}
function response(data,msg){
    if(data.indexOf('OK') >= 0){
        $('#galleryMessage').show().html(msg);          
        fetchData();
        $('#galleryForm')[0].reset();

        window.setTimeout(function(){
            $('#myModal').modal('toggle');
            $('#galleryMessage').hide().html('');
        },2000);
    }else{
        $('#galleryMessage').show().html('Something Went Wrong');
        window.setTimeout(function(){
            $('#galleryMessage').hide().html('');
        },3000);
    }
}

$('#addNew').on({
    'click' : function(){
        $('#galleryForm')[0].reset();

        $('#addBtn').show();
        $('#upBtn').hide();
    }
});
$('#galleryForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#galleryMessage').fadeIn().html('Checking Data');
        var formdata= new FormData(this);
        $.ajax({

            url : 'req/add_abgallery.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
                // console.log(data);           
                response(data,'Successfully Inserted');
            fetchData();
        }   
    });
    }
});

function delete_abgallery(id) {
    $('#dModal').modal('toggle');
    $('#yes').attr('onclick','del('+id+')');

}

function del(id){
    $.post('req/delete_abgallery.php', {ab_id : id}, function(data){
        /*if(data == 'OK'){*/
            if(data.indexOf("1") >= 0 ){
                $('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
                window.setTimeout(function(){
                    $('#dModal').modal('toggle');
                    $('#tr-'+id).remove();
                },1000);
            }else{
                $('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');         
            }

            window.setTimeout(function(){
                $('#dAlert').addClass('hidden').html('');
            },1000);
        });
}
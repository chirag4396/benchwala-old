fetchData();
function fetchData(){
    $.ajax({
        url : 'req/testimonial/fetch_testimonial.php',
        type : 'post',
        data : '',
        dataType : 'json',
        success : function(data){
             console.log(data);
            trArr = [];

            $.each(data,function(key,val){
                tr = '<tr id = "tr-'+val.test_id+'">'
                +'<td>'+val.test_id+'</td>'
                +'<td>'+val.test_name+'</td>'
                +'<td>'+val.test_designation+'</td>'
                +'<td>'+val.test_description+'</td>'
                +'<td>'+'<img src="../uploads/'+val.test_image+'" width="100" class="img-thumbnail"/>'+'</td>'
                +'<td>'
                +'<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getData('+val.test_id+')">Edit</button>|'
                +'<button class="btn btn-danger" onclick="delete_testimonial('+val.test_id+')">DELETE</button>'
                +'</td>'
                +'</tr>';
                trArr.push(tr);
            });

            $('#testTable tbody').html(trArr);
        }
    });

}
function response(data,msg){
    if(data.indexOf('OK') >= 0){
        $('#testMessage').show().html(msg);          
        fetchData();
        $('#testForm')[0].reset();

        window.setTimeout(function(){
            $('#myModal').modal('toggle');
            $('#testMessage').hide().html('');
        },2000);
    }else{
        $('#testMessage').show().html('Something Went Wrong');
        window.setTimeout(function(){
            $('#testMessage').hide().html('');
        },3000);
    }
}

$('#addNew').on({
    'click' : function(){
        $('#testForm')[0].reset();

        $('#addBtn').show();
        $('#upBtn').hide();
    }
});
$('#testForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#testMessage').fadeIn().html('Checking Data');
        var formdata= new FormData(this);
        // formdata.append('desc',CKEDITOR.instances['desc'].getData());
        $.ajax({

            url : 'req/testimonial/add_testimonial.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
                console.log(data);           
                response(data,'Successfully Inserted');
                fetchData();
            }   
        });
    }
});

function getData(id){
    $('#addBtn').hide();
    $('#upBtn').show();
    // $('#category').show();
    $.ajax({
        url : 'req/testimonial/testimonialDetails.php',
        type : 'post',
        dataType : 'json',
        data : {'id':id},
        success : function(data){
            // console.log(data);
            $('#id').val(data.test_id);
            $('#test_name').val(data.test_name);
            $('#test_designation').val(data.test_designation);
            $('#test_desc').val(data.test_description);
            $('#image').val(data.test_image);
        }
    });
}

$('#upBtn').on({
    'click' : function(e){
        e.preventDefault();
        var formdata = new FormData($('#testForm')[0]);
        $.ajax({
            url : 'req/testimonial/update_testimonial.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
            //  console.log(data);   
            response(data,'Successfully Updated');
            fetchData();
        }   
    });
    }
});
function delete_testimonial(id) {
    $('#dModal').modal('toggle');
    $('#yes').attr('onclick','del('+id+')');

}

function del(id){
    $.post('req/testimonial/delete_testimonial.php', {test_id : id}, function(data){
        /*if(data == 'OK'){*/
            if(data.indexOf("1") >= 0 ){
                $('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
                window.setTimeout(function(){
                    $('#dModal').modal('toggle');
                    $('#tr-'+id).remove();
                },1000);
            }else{
                $('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');         
            }

            window.setTimeout(function(){
                $('#dAlert').addClass('hidden').html('');
            },1000);
        });
}
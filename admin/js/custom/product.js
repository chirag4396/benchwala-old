fetchData();
function fetchData(){
    $.ajax({
        url : 'req/product/fetch_product.php',
        type : 'post',
        data : '',
        dataType : 'json',
        success : function(data){
            // console.log(data);
            trArr = [];

            $.each(data,function(key,val){
                tr = '<tr id = "tr-'+val.pro_id+'">'
                +'<td>'+val.pro_id+'</td>'
                +'<td>'+val.pro_model_no+'</td>'
                +'<td>'+val.pro_size+'</td>'
                +'<td>'+val.pro_description+'</td>'
                +'<td>'+'<img src="../uploads/'+val.pro_image+'" width="100" class="img-thumbnail"/>'+'</td>'
                +'<td>'
                +'<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="getData('+val.pro_id+')">Edit</button>|'
                +'<button class="btn btn-danger" onclick="delete_product('+val.pro_id+')">DELETE</button>'
                +'</td>'
                +'</tr>';
                trArr.push(tr);
            });

            $('#productTable tbody').html(trArr);
            $('#productTable').DataTable().destroy();
            $('#productTable').DataTable({
                responsive: true
            });
        }
    });

}
function response(data,msg){
    if(data.indexOf('OK') >= 0){
        $('#productMessage').show().html(msg);          
        fetchData();
        $('#productForm')[0].reset();

        window.setTimeout(function(){
            $('#myModal').modal('toggle');
            $('#productMessage').hide().html('');
        },2000);
    }else{
        $('#productMessage').show().html('Something Went Wrong');
        window.setTimeout(function(){
            $('#productMessage').hide().html('');
        },3000);
    }
}

$('#addNew').on({
    'click' : function(){
        $('#productForm')[0].reset();

        $('#addBtn').show();
        $('#upBtn').hide();
    }
});
$('#productForm').on({
    'submit' : function(e){
        e.preventDefault();
        $('#productMessage').fadeIn().html('Checking Data');
        var formdata= new FormData(this);
        formdata.append('desc',CKEDITOR.instances['desc'].getData());
        formdata.append('spec',CKEDITOR.instances['spec'].getData());
        $.ajax({

            url : 'req/product/add_product.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
            // console.log(data);           
            response(data,'Successfully Inserted');
            fetchData();
        }   
    });
    }
});

function getData(id){
    $('#addBtn').hide();
    $('#upBtn').show();
    $('#category').show();
    $.ajax({
        url : 'req/product/productDetails.php',
        type : 'post',
        dataType : 'json',
        data : {'id':id},
        success : function(data){
            
            $desc = CKEDITOR.instances['desc'].setData(data.pro_description);
            $spec = CKEDITOR.instances['spec'].setData(data.pro_specification);
            $('#id').val(data.pro_id);
            $('#model_no').val(data.pro_model_no);
            $('#size').val(data.pro_size);
            $('#frame').val(data.pro_frame);
            $('#top').val(data.pro_top);
            $('#category').val(data.pro_cat_id+'#'+data.cat_name.replace(/\s/g,'-'));
            $('#cate').val(data.pro_cat_id);
            $('#dis_price').val(data.pro_dis_price);
            $('#price').val(data.pro_price);
            
            $('#imagePreview').attr('src','../../uploads/'+data.pro_image);
        }
    });
}

$('#upBtn').on({
    'click' : function(e){
        e.preventDefault();
        
        var formdata = new FormData($('#productForm')[0]);
        // console.log(formdata);
        formdata.append('desc',CKEDITOR.instances['desc'].getData());
        formdata.append('spec',CKEDITOR.instances['spec'].getData());
        $.ajax({
            url : 'req/product/update_product.php',
            type : 'post',
            data : formdata,
            contentType:false,
            processData:false,
            success: function(data){    
            // console.log(data);   
            response(data,'Successfully Updated');
            fetchData();
        }   
    });
    }
});

function delete_product(id) {
    $('#dModal').modal('toggle');
    $('#yes').attr('onclick','del('+id+')');

}

function del(id){
    $.post('req/product/delete_product.php', {pro_id : id}, function(data){
        //if(data == 'OK'){
            if(data.indexOf("1") >= 0 ){
                $('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
                window.setTimeout(function(){
                    $('#dModal').modal('toggle');
                    $('#tr-'+id).remove();
                },1000);
            }else{
                $('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');         
            }

            window.setTimeout(function(){
                $('#dAlert').addClass('hidden').html('');
            },1000);
        });
}

var height,width;

function imageUpload(id) {

    $('#'+id).on("change", function(){
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) return;

        if (/^image/.test( files[0].type)){
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            var sizeMB = files[0].size / (1024 * 1024);
            if(sizeMB > 2){
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', true);
                $('#'+id).parent().append('<p style = "color: red;margin-top: 10px;">Please select lessthan 2 MB Image.</p>');
            }else{
                $('#'+id).parent().find('p').remove();
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', false);             
            }
            reader.onloadend = function(upImg){

                $("#"+id+"Preview").attr("src", this.result);
                urlValue = this.result;
                
                var image = new Image();
                image.src = upImg.target.result;

                image.onload = function() {
                    height = this.height;
                    width = this.width;                 
                };
            }                
        }
    });
}

imageUpload('image');
<?php
include("../config.php");
?>
<!DOCTYPE html>
<head>
	<title>Benchwala</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>

	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>


	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- tables -->
				
				<div class="table-heading">
					<h2></h2>
				</div>
				<div class="agile-tables">
					<div class="w3l-table-info">
						<h3>Contact Details</h3>
						<table id="table">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Subject</th>
									<th>Message</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$res = $conn->query('SELECT * from  contact_details');	
								if($res->num_rows){
									while ($row = $res->fetch_assoc())
									{
										$c_id=$row['c_id'];

										?>
										<tr id = "tr-<?php echo $row['c_id'];?>">
											<td class="center"><?php echo $row['c_id'];?></td>
											<td class="center"><?php echo $row['c_fname'];?></td>
											<td class="center"><?php echo $row['c_lname'];?></td>
											<td class="center"><?php echo $row['c_email'];?></td>
											<td class="center"><?php echo $row['c_subject'];?></td>
											<td class="center"><?php echo $row['c_message'];?></td>
											
											<td style="width:3%;" ><button class="btn btn-primary" onclick="delete_contact(<?php echo $row['c_id'];?>);">Delete</button></td>
											<td></td>
											

										</tr>
									</tbody>
									<?php } }?>
								</table>

			 <!-- <code class="js">
					$('#table').basictable();
				  </code>
				-->

			</div>
			<!-- //tables -->
		</div>
	</div>
	
	<div id="dModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				
				<div class="modal-body text-center">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p>Are you sure ?</p>	        
					<button type="button" class="btn btn-danger" id = "yes">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
					<div id = "dAlert" class="alert text-center hidden"></div>
				</div>
			</div>

		</div>
	</div>
	<!-- footer -->
	<?php require("footer.php") ?>
	<!-- //footer -->
</section>
<script src="js/bootstrap.js"></script>

<script>
	function delete_contact(id) {
		$('#dModal').modal('toggle');
		$('#yes').attr('onclick','del('+id+')');
		
	}

	function del(id){
		$.post('req/delete_contact.php', {c_id : id}, function(data){
			/*if(data == 'OK'){*/
				if(data.indexOf("1") >= 0 ){
					$('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
					window.setTimeout(function(){
						$('#dModal').modal('toggle');
						$('#tr-'+id).remove();
					},1000);
				}else{
					$('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');    		
				}

				window.setTimeout(function(){
					$('#dAlert').addClass('hidden').html('');
				},1000);
			});
	}
</script>
</body>
</html>


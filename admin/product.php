<?php
include("../config.php");
?>
<!DOCTYPE html>
<head>
	<title>Benchwala</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>
	<!-- <script src="js/ckeditor.js"></script> -->
	<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>
	<script type="text/javascript">
		$(function () {
			$("#chkPassport").click(function () {
				if ($(this).is(":checked")) {
					$("#dvPassport").show();
				} else {
					$("#dvPassport").hide();
				}
			});
		});
	</script>

	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	
	<style type="text/css">
		#imagePreview {
			object-fit: contain;
			width: 200px;
		}
	</style>
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- tables -->
				
				<div class="table-heading">
					<h2></h2>
				</div>
				<div class="agile-tables">
					<div class="w3l-table-info">
						<h3>Product Details</h3>
						<div class="col-sm-12">
							<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" id = "addNew">Add New Record</button>
							<!-- <button class="btn btn-danger" id = "deleteAll">Delete All</button> -->
							<div class="clear"></div>
						</div>	
						<!-- <a href="product.php"  class="btn btn-primary pull-right" name="addbtn" id="addbtn" >Add Product</a> -->
						<table id="productTable">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>Modal No</th>
									<th>Size</th>
									<th>Description</th>
									<th>Product Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>

						</table>

			 <!-- <code class="js">
					$('#table').basictable();
				  </code>
				-->

			</div>
			<!-- //tables -->
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form id = "productForm">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Product Form</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="hidden" id = "id" name = "id">
						<div class="form-group">
							<label for="exampleInputEmail1">Model No.</label>
							<input type="text" name = "modal_no" id="model_no" placeholder="Enter Modal Number" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Product Size</label>
							<input type="text" name="size" class="form-control" id="size" placeholder="Enter Product Size">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Product Frame</label>
							<input type="text" name="frame" class="form-control" id="frame" placeholder="Enter Product Frame">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Product Top</label>
							<input type="text" name="top" class="form-control" id="top" placeholder="Enter Product Top" required>
						</div>
						<div class="form-group"> 
							<div class="form-group"> 
								<label for="selector1">Select Category</label>
								<input type="hidden" name="category" id = "cate">
								<select id="category" class="form-control">
									<option>Select Category</option>
									<?php
									$sql = "select * from category"; 
									$qry = $conn->query($sql);
									while ($row=$qry->fetch_assoc()) 
									{

										echo '<option value="'.$row['cat_id'].'#'.preg_replace('/\s/','-',$row['cat_name']).'">'.$row['cat_name'].'</option>';
									}
									?>

								</select>

							</div>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Description</label>
							<textarea name="desc" id="desc" placeholder="Enter Description"></textarea>
							<script>
								CKEDITOR.replace( 'desc' );
							</script>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Specifications</label>
							<textarea name="spec" id="spec" placeholder="Enter Specification"></textarea>
							<script>
								CKEDITOR.replace( 'spec' );
							</script>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Discound Price</label>
							<input type="text" name="dis_price" class="form-control" id="dis_price" placeholder="Enter Product Discound Price">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Price</label>
							<input type="text" name="price" class="form-control" id="price" placeholder="Enter Product Price">
						</div>
						<div class="form-group">
							<img src="images/no-image.png" width="300" height="150" id = "imagePreview">
							<div class="clearfix"></div>
							<input type = "file" id ="image" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "image">		
							<label class="btn btn-success" for = "image">Choose Image</label>	
							<!-- <input type="file" name="image" id="image"> -->
						</div>
					</div>
					<div class="modal-footer">						
						<button type="submit" class="btn btn-primary" id = "addBtn">Add</button>
						<button type="button" class="btn btn-primary" id = "upBtn">Update</button>
					</div>
				</form>
				
				<div class="alert alert-warning text-md-center" id = "productMessage" style="display: none;">				
				</div>

			</div>
		</div>
	</div>

	<div id="dModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body text-center">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p>Are you sure ?</p>	        
					<button type="button" class="btn btn-danger" id = "yes">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
					<div id = "dAlert" class="alert text-center hidden"></div>
				</div>
			</div>

		</div>
	</div>
	<!-- footer -->
	<?php require("footer.php") ?>
	<!-- //footer -->
</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	

<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="../admin/js/custom/product.js"></script>
<script type="text/javascript">
	$('#category').on({
		'change' : function(){
			var cat = ['videos', 'Videos', 'Video', 'Videos'];
			var v = this.value.split('#');
			if($.inArray(v[1],cat) > 0){
				$('#dvPassport').show();
				$('#msgI').html('(optional)');
			}else{
				$('#dvPassport').hide();
				$('#msgI').html('');
			}

			$('#cate').val(v[0]);
		}
	});
</script>

</body>
</html>


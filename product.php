<?php 
include 'header.php'; 
?>
<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/q5.jpg);">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box" data-animate-effect="fadeIn">
						<h1>Products</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div id="fh5co-product">
	<div class="row animate-box">
		<div class="col-md-12  text-center fh5co-heading">

			<p>Get a fresh, perfectly coordinated look for your Place with these gorgeous new designs.</p>

		</div>
	</div>

	<div class="container pro">

		<div class="row">
			<div class="col-md-3">
				<div class="widget category-list">
					<h4 class="widget-header">All Category</h4>
					<ul class="category-list" id = "categories">
					<?php 
					$res = $conn->query('select * from category');
						// if($res->num_rows){
                                    // $k = 0;
					while ($row2 = $res->fetch_assoc()) 
					{
						echo '<li  data-id = "'.$row2['cat_id'].'" ><a href="javascript:;">'.$row2['cat_name'].
						'</a></li>';
					}
						// }
					?>
					
				</ul>
			</div>
		</div>
		<div class="col-md-9 ">
			<h4 class="widget-header col-md-8 col-xs-12">ALL PRODUCTS</h4>

			<div class="col-md-4 col-xs-12"><span style="color:red">Sort By</span>
				<select id = "sort">

					<option value="model">Model No.</option>

					<option value="desc">Price High to Low</option>
					<option value="asc">Price Low to High</option>


				</select>
			</div>

			<div class="clear"></div>
			<h6><span style="color:red">Purchase Order:</span> We do accept PO from Office; Corporate; Institute & Hospitals.</h6>

			<div id="product_box">

			</div>

		</div>
	</div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog" >
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h2 class="head1">Enquiry</h2>
				<div class="border-line3"></div>
			</div>
			<div class="modal-body">
				<form name="myform" id="enquiryForm">
					<input type="hidden" name="id" id="id">
					<div class="row  form-group">	
						<div class="col-md-4">
							<label for="firstname"> Name <span>*</span></label>
						</div>
						<div class="col-md-8">
							<input type="text" name="firstname" placeholder="Name" class="form-control" required="">
									<!-- <input type="hidden" name="code" value="AU-002">
									<input type="hidden" name="cat_name" value="Auditorium Chair">
									<input type="hidden" name="pcat_pkey" value="318">
									<input type="hidden" name="list_price" value="11,000.00">
									<input type="hidden" name="your_price" value="6,875.00"> -->
								</div>
							</div>	

							<div class="row form-group">
								<div class="col-md-4">
									<label for="email"> Email<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="email" name="email" placeholder="Email" class="form-control" required="">
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Contact No<span>*</span></label>
								</div>
								<div class="col-md-8">
									<input type="tel" pattern="^\d{10}$" name="mobile" placeholder="Contact No" class="form-control" required="">
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4">
									<label for="phone">Message<span>*</span></label>
								</div>
								<div class="col-md-8">
									<textarea type="text" name="message" placeholder="Enter Your message" class="form-control" required=""></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="form-group col-md-8 col-xs-12">
								<input type="submit" value="Send Message" class="btn btn-primary">
							</div>
						</div>
					</form>
					<div class="alert alert-warning text-md-center" id = "enquiryMessage" style="display: none;">				
					</div>
					<div class="clear"></div>
					<div class="top-header">
						<div class="">
							<div class="col-md-12" >
								<p>CALL:<?php $qry = 'select ad_city,ad_mobile from address_details';
									$res = $conn->query($qry);
									if($res->num_rows){
										while($row = $res->fetch_assoc())
										{
											echo $row['ad_city'].':'.$row['ad_mobile'].'|';
										}
									}  ?></p>
								</div>
								<div class="col-md-12">
									<p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - 
										<?php $qry = 'select ad_email from address_details where ad_city="PUNE(HO)"';
										$res = $conn->query($qry);
										if($res->num_rows){
											while($row = $res->fetch_assoc())
											{
												echo $row['ad_email'];
											}
										}  ?></p>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- jQuery -->
			</div>

			<?php include 'footer.php'; ?>
			<script type="text/javascript">
				var catId = '<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>';
			</script>
			<script src="js/custom/product.js"></script>
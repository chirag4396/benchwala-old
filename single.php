<?php
include 'header.php';
$id = $_GET['id'];
?>

<div id="fh5co-about pad-t" class="pad-t">

  <div class="container p-details pad-t">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Products</a></li>
        <li class="breadcrumb-item active" aria-current="page">Class room furniture</li>
      </ol>
    </nav>
    <?php
    $res1 = $conn->query('select * from product_details where pro_id='.$_GET['id']);

    if($res1->num_rows){

      while ($row1 = $res1->fetch_assoc() ) 
      {
        // print_r($row1);
        // echo "OK";
        // echo '<h4 class="widget-header  col-xs-12"><span style="color: #000">Model no :</span>'.$row1['pro_model_no'].'</h4>';
        ?>
        <div class="row single1" style="border-bottom: :1px dashed #ccc;">



          <div class="clear"></div>
          <div class="col-md-6">
            <img style="width:100%; border-right:1px dashed #ccc;" class="img-fluid" src="uploads/<?php echo $row1['pro_image']; ?>" alt="">
          </div>

          <div class="col-md-6">
            <h4 class="widget-header  col-xs-12"><span style="color: #000">Model no :</span><?php echo $row1['pro_model_no']; ?></h4>

            <button style="color:#ccc;" class="btn btn-primary btn-outline btn-lg "> <strike>Price:<?php echo $row1['pro_price']; ?></strike></button>  
            <button class="btn btn-primary btn-outline btn-lg">Discount  Price:<?php echo $row1['pro_dis_price']; ?></button>  
            <div class="widget category-list">

              <ul class="category-list">
                <li><a>Size:<?php echo $row1['pro_size']; ?></a></li>
                <li><a>Frame:<?php echo $row1['pro_frame']; ?></a></li>
                <li><a>Top:<?php echo $row1['pro_top']; ?></a></li>
              </ul>
            </div>
            <button class="btn btn-primary btn-outline btn-lg" onclick="productEnquiry(<?php echo $row1['pro_id']; ?>)">Enquiry</button>
             
            <div class="clearfix"></div>
          </div>    
        </div>


      <!--  <div class="col-md-12 ">-->
      <!--    <div class="fh5co-tabs animate-box fadeInUp animated-fast">-->
      <!--      <ul class="fh5co-tab-nav">-->
      <!--        <li class=""><a href="#" data-tab="1"><span class="icon visible-xs"><i class="icon-file"></i></span><span class="hidden-xs">Product Details</span></a></li>-->
      <!--        <li class=""><a href="#" data-tab="2"><span class="icon visible-xs"><i class="icon-bar-graph"></i></span><span class="hidden-xs">Specification</span></a></li>-->
      <!--        <li class="active"><a href="#" data-tab="3"><span class="icon visible-xs"><i class="icon-star"></i></span><span class="hidden-xs">Feedback &amp; Ratings</span></a></li>-->
      <!--      </ul>-->

            <!-- Tabs -->
      <!--      <div class="fh5co-tab-content-wrap" style="height: 504px;">-->

      <!--        <div class="fh5co-tab-content tab-content active fadeIn animated-fast fadeOutDown" data-tab-content="1">-->
      <!--          <div class="col-md-10 col-md-offset-1">-->

      <!--            <?php echo $row1['pro_description']; ?>-->

      <!--          </div>-->
      <!--        </div>-->

      <!--        <div class="fh5co-tab-content tab-content active fadeIn animated-fast fadeOutDown" data-tab-content="2">-->
      <!--          <div class="col-md-10 col-md-offset-1">-->
      <!--           <?php echo $row1['pro_specification']; ?>-->
      <!--         </div>-->
      <!--       </div>-->

      <!--       <div class="fh5co-tab-content tab-content active fadeIn animated-fast" data-tab-content="3">-->
      <!--        <div class="col-md-10 col-md-offset-1">-->
      <!--          <h3>Happy Buyers</h3>-->
      <!--          <div class="feed">-->
      <!--            <div>-->
      <!--              <blockquote>-->
      <!--                <p>Paragraph placeat quis fugiat provident veritatis quia iure a debitis adipisci dignissimos consectetur magni quas eius nobis reprehenderit soluta eligendi quo reiciendis fugit? Veritatis tenetur odio delectus quibusdam officiis est.</p>-->
      <!--              </blockquote>-->
      <!--              <h3>— Louie Knight</h3>-->
      <!--              <span class="rate">-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--              </span>-->
      <!--            </div>-->
      <!--            <div>-->
      <!--              <blockquote>-->
      <!--                <p>Paragraph placeat quis fugiat provident veritatis quia iure a debitis adipisci dignissimos consectetur magni quas eius nobis reprehenderit soluta eligendi quo reiciendis fugit? Veritatis tenetur odio delectus quibusdam officiis est.</p>-->
      <!--              </blockquote>-->
      <!--              <h3>— Joefrey Gwapo</h3>-->
      <!--              <span class="rate">-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--                <i class="icon-star2"></i>-->
      <!--              </span>-->
      <!--            </div>-->
      <!--          </div>-->
      <!--        </div>-->
      <!--      </div>-->
      <!--    </div>-->

      <!--  </div>-->
      <!--</div>-->
    </div>

    <?php
  }
}
?>

<div class="col-xs-12 pad-t mar-b">
<!-- <h2 class="head1">Recently Searched... </h2>-->
 <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:150px;overflow:hidden;visibility:hidden;">

  <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg" />
  </div>
  <!--<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:150px;overflow:hidden;">
    <div data-p="43.75">
      <a href="product.php"><img data-u="image" src="images/001.jpg" /></a>
    </div>
    <div data-p="43.75">
     <a href="product.php"><img data-u="image" src="images/005.jpg" /></a>
   </div>
   <div data-p="43.75">
     <a href="product.php"><img data-u="image" src="images/006.jpg" /></a>
   </div>
   <div data-p="43.75">
     <a href="product.php"><img data-u="image" src="images/007.jpg" /></a>
   </div>
   <div data-p="43.75">
     <a href="product.php"><img data-u="image" src="images/008.jpg" /></a>
   </div>
   <div data-p="43.75">
     <a href="product.php"><img data-u="image" src="images/009.jpg" /></a>
   </div>

 </div>-->
 <!-- Bullet Navigator -->
 <div data-u="navigator" class="jssorb057" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
  <div data-u="prototype" class="i" style="width:16px;height:16px;">
    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
      <circle class="b" cx="8000" cy="8000" r="5000"></circle>
    </svg>
  </div>
</div>
<!-- Arrow Navigator -->
<div data-u="arrowleft" class="jssora073" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
  <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
    <path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>
  </svg>
</div>
<div data-u="arrowright" class="jssora073" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
  <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
    <path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>
  </svg>
</div>
</div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog" >
  <div class="modal-dialog" ">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h2 class="head1">Enquiry</h2>
        <div class="border-line3"></div>
      </div>
      <div class="modal-body">
        <form name="myform" id="enquiryForm">
          <input type="hidden" name="id" id="id">
          <div class="row  form-group"> 
            <div class="col-md-4">
              <label for="firstname"> Name <span>*</span></label>
            </div>
            <div class="col-md-8">
              <input type="text" name="firstname" placeholder="Name" class="form-control" required="">
                  <!-- <input type="hidden" name="code" value="AU-002">
                  <input type="hidden" name="cat_name" value="Auditorium Chair">
                  <input type="hidden" name="pcat_pkey" value="318">
                  <input type="hidden" name="list_price" value="11,000.00">
                  <input type="hidden" name="your_price" value="6,875.00"> -->
                </div>
              </div>  

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="email"> Email<span>*</span></label>
                </div>
                <div class="col-md-8">
                  <input type="email" name="email" placeholder="Email" class="form-control" required="">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4">
                  <label for="phone">Contact No<span>*</span></label>
                </div>
                <div class="col-md-8">
                  <input type="tel" name="mobile" pattern="^\d{10}$"  placeholder="Contact No" class="form-control" required="">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-4">
                  <label for="phone">Message<span>*</span></label>
                </div>
                <div class="col-md-8">
                  <textarea type="text" name="message" placeholder="Enter Your message" class="form-control" required=""></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div class="form-group col-md-8 col-xs-12">
                <input type="submit" value="Send Message" class="btn btn-primary">
              </div>
            </div>
          </form>
          <div class="alert alert-warning text-md-center" id = "enquiryMessage" style="display: none;">       
          </div>
          <div class="clear"></div>
          <div class="top-header">
            <div class="">
              <div class="col-md-12" >
                <p>CALL:<?php $qry = 'select ad_city,ad_mobile from address_details';
                  $res = $conn->query($qry);
                  if($res->num_rows){
                    while($row = $res->fetch_assoc())
                    {
                      echo $row['ad_city'].':'.$row['ad_mobile'].'|';
                    }
                  }  ?></p>
                </div>
                <div class="col-md-12">
                  <p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - 
                    <?php $qry = 'select ad_email from address_details where ad_city="PUNE(HO)"';
                    $res = $conn->query($qry);
                    if($res->num_rows){
                      while($row = $res->fetch_assoc())
                      {
                        echo $row['ad_email'];
                      }
                    }  ?></p>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- jQuery -->
      </div>
      <?php include 'footer.php'; ?>
      <script type="text/javascript">
				var catId = '<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>';
			</script>
			<script src="js/custom/product.js"></script>
<?php 
include 'config.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Benchwala </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		jssor_1_slider_init = function() {

			var jssor_1_SlideshowTransitions = [
			{$Duration:1200,$Zoom:1,$Easing:{$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad},$Opacity:2},
			{$Duration:1000,$Zoom:11,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
			{$Duration:1200,$Zoom:1,$Rotate:1,$During:{$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
			{$Duration:1000,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InQuint,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
			{$Duration:1200,x:0.5,$Cols:2,$Zoom:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
			{$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
			{$Duration:1200,x:0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Opacity:2,$Round:{$Rotate:0.5}},
			{$Duration:1000,x:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InQuint,$Zoom:$Jease$.$InQuart,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
			{$Duration:1200,x:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Opacity:2,$Round:{$Rotate:0.5}},
			{$Duration:1000,x:4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InQuint,$Zoom:$Jease$.$InQuart,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
			{$Duration:1200,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
			{$Duration:1000,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
			{$Duration:1200,x:-4,y:2,$Rows:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
			{$Duration:1200,x:1,y:2,$Cols:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:19},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.8}}
			];

			var jssor_1_options = {
				$AutoPlay: 1,
				$SlideshowOptions: {
					$Class: $JssorSlideshowRunner$,
					$Transitions: jssor_1_SlideshowTransitions,
					$TransitionsOrder: 1
				},
				$ArrowNavigatorOptions: {
					$Class: $JssorArrowNavigator$
				},
				$ThumbnailNavigatorOptions: {
					$Class: $JssorThumbnailNavigator$,
					$Rows: 2,
					$SpacingX: 14,
					$SpacingY: 12,
					$Orientation: 2,
					$Align: 156
				}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			/*#region responsive code begin*/

			var MAX_WIDTH = 960;

			function ScaleSlider() {
				var containerElement = jssor_1_slider.$Elmt.parentNode;
				var containerWidth = containerElement.clientWidth;

				if (containerWidth) {

					var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

					jssor_1_slider.$ScaleWidth(expectedWidth);
				}
				else {
					window.setTimeout(ScaleSlider, 30);
				}
			}

			ScaleSlider();

			$Jssor$.$AddEvent(window, "load", ScaleSlider);
			$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			/*#endregion responsive code end*/
		};
	</script>

</head>
<body>

	<body>
		
		<div class="fh5co-loader" style="display: none;"></div>

		<div id="page"><a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle fh5co-nav-white"><i></i></a><div id="fh5co-offcanvas">
			<ul>
				<li>
					<a href="index.php">Home</a>

				</li>
				<li><a href="about.php">About us</a></li>
				<li>
					<a href="product.php">Products</a>

				</li>
				<li>
					<a href="gallery.php">Gallery</a>

				</li>

				<li>
					<a href="feedback.php">Feedback form</a>

				</li>

				<li><a href="Steelfab.pdf" target="_blank">
					Download Catalogue
				</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>
		<nav class="fh5co-nav navbar-fixed-top" role="navigation">

			<div class="top-header">
				<div class="container">
					<div class="col-md-9">
						<p>CALL: <?php $qry = 'select ad_city,ad_mobile from address_details';
							$res = $conn->query($qry);
							if($res->num_rows){
								while($row = $res->fetch_assoc())
								{
									echo $row['ad_city'].':'.$row['ad_mobile'].'|';
								}
							}  ?></p>
						</div>
						<div class="col-md-3">
							<p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - 
								<?php $qry = 'select ad_email from address_details where ad_city="PUNE(HO)"';
								$res = $conn->query($qry);
								if($res->num_rows){
									while($row = $res->fetch_assoc())
									{
										echo $row['ad_email'];
									}
								}  ?></p>
							</div>
						</div>
					</div>
					<div class=" top-header2">
						<div class="row">
							<div class="col-md-3 col-xs-2">
								<div id="fh5co-logo"><a href="index"><img src="images/logoo.png"></a></div>
							</div>
							<div class="col-md-7 col-xs-6 text-center menu-1 no-padding">
								<ul>
									<li>
										<a href="index">Home</a>

									</li>
									<li><a href="about">About us</a></li>
									<li>
										<a href="product">Products</a>

									</li>
									<li>
										<a href="gallery">Gallery</a>

									</li>

									<li>
										<a href="feedback">Feedback form</a>

									</li>


									<li><a href="Steelfab.pdf" target="_blank">
										Download Catalogue
									</a></li>
									<li><a href="contact">Contact</a></li>
								</ul>
							</div>
							<div class="col-md-2 col-xs-4 text-right hidden-xs menu-2">
								<form id="searchForm">
									<ul>
										<li class="search">

											<div class="input-group">

												<form id = "searchForm">

													<input type="text" name="sname" id="content" placeholder="Search..">
													<span class="input-group-btn">
														<button class="btn btn-primary" type="submit" id="bsearch"><i class="icon-search"></i></button>
													</span>
												</form>
											</div>


										</li>

									</ul>
								</form>
							</div>
						</div>

					</div>
				</nav>
				<!-- <div id="search_product" style="display: none;"></div> -->